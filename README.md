<h1 align="center">
![noevim](https://neovim.io/images/logo@2x.png)
</h1>

# Neovim Personal setup

## Requires
- Neovim
- The Silver Searcher
- Powerline fonts

## Install

###### Cloning the repository

```
$ git clone git@gitlab.com:oifelipesan/nvim.git ~/.config/nvim
```

###### Installing plugins
```
$ cd ~/.config/nvim
$ nvim init.vim
:PlugInstall
```
